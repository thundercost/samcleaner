"""
SamCleaner config load functions

Copyright (C) 2019

This file is part of SamCleaner.

SamCleaner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SamCleaner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SamCleaner.  If not, see <http://www.gnu.org/licenses/>.
"""

# Import required modules
import logging, config

def load():
	"""Load configuration from the samcleaner.conf file"""

	try:

		# Read configuration
		rc = config.cfg.read("samcleaner.conf")

		# Verify returned list
		if len(rc)!=1 or rc[0]!="samcleaner.conf":
			logging.error("config load(): failed to load the configuration file: samcleaner.conf")
			quit()

	except:

		logging.error("config load(): failed to load the configuration file: samcleaner.conf")
		quit()

