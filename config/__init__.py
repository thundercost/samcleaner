"""
SamCleaner config functions

Copyright (C) 2019

This file is part of SamCleaner.

SamCleaner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SamCleaner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SamCleaner.  If not, see <http://www.gnu.org/licenses/>.
"""

# Import required modules
import sys, logging, os, configparser
from config.load import *
from config.save import *

# Pointer to module self
this = sys.modules[__name__]

# Logging (disabled)
#logging.basicConfig(level=logging.INFO)

# Application preferences
this.cfg = configparser.ConfigParser()

# Default values for application preferences
this.cfg['samcleaner'] = {
	'adb': "/usr/bin/adb",
	'ssh': ""
}

# Runtime preferences
this.runtime = configparser.ConfigParser()

# Default values for runtime preferences
this.runtime['samcleaner'] = {
	'currentDeviceID': "",
	'command': ""
}

# Adb status flag (unknown, error, ok)
this.status = "unknown"

if os.path.isfile("samcleaner.conf")==False:

	logging.info("config: configuration file 'samcleaner.conf' not found")

	config.save()

	logging.info("config: default configuration file 'samcleaner.conf' created")

else:

	logging.info("config: loading configuration file 'samcleaner.conf'")

	config.load()

def setStatus(newStatus):
	"""Set a new status indicator"""

	this.status = newStatus

def getStatus():
	"""Get the current status indicator"""

	return this.status

